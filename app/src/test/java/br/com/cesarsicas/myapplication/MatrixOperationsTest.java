package br.com.cesarsicas.myapplication;

import android.util.Log;

import org.junit.Test;
import org.junit.Assert.*;

import static junit.framework.Assert.assertEquals;

/**
 * Created by julio on 17/07/17.
 */

public class MatrixOperationsTest {


    @Test
    public void test1() {
        int[][] matrix = {{1}};

        MatrixOperations mo = new MatrixOperations();

        assertEquals(1, mo.somaMaxCaminho(matrix));

    }


    @Test
    public void test2() {
        int[][] matrix = {{1, 2, 3}};

        MatrixOperations mo = new MatrixOperations();

        assertEquals(6, mo.somaMaxCaminho(matrix));

    }

    @Test
    public void test3() {
        int[][] matrix = {
                {1, 2, 3},
                {4, 5, 6}};

        MatrixOperations mo = new MatrixOperations();
        assertEquals(16, mo.somaMaxCaminho(matrix));

    }

    @Test
    public void test4() {
        int[][] matrix = {
                {10, 3, 6},
                {4, 3, 9},
                {1, 10, 0}};

        MatrixOperations mo = new MatrixOperations();
        assertEquals(28, mo.somaMaxCaminho(matrix));

    }


}
