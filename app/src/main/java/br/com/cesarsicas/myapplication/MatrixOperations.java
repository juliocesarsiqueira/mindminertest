package br.com.cesarsicas.myapplication;

/**
 * Created by julio on 17/07/17.
 */

public class MatrixOperations {

    public int somaMaxCaminho(int[][] grid) {

        int ultimaLinha =  grid.length-1;
        int ultimaColuna = grid[ultimaLinha].length-1;



        int ultimoElemento = grid[ultimaLinha][ultimaColuna];
        int somaFinal = ultimoElemento;


        for (int l = (grid.length - 1); l >= 0; ) {

            for (int c = (grid[l].length - 1); c >= 0; ) {

                if(podeEsquerda(grid,l,c) && podeCima(grid,l,c)){
                    //ver qual dos dois e maior

                    if(grid[l-1][c] > grid[l][c-1] ){
                        somaFinal+=grid[l-1][c];
                        l--;
                        continue;
                    }
                    else{
                        somaFinal+=grid[l][c-1];
                        c--;
                        continue;
                    }

                }

                if(podeEsquerda(grid,l,c) && !podeCima(grid,l,c)){
                    //andar pra esquerda

                    somaFinal+=grid[l-1][c];
                    l--;
                    continue;

                }
                if(! podeEsquerda(grid,l,c) && podeCima(grid,l,c)){
                    //andar pra cima
                    somaFinal+=grid[l][c-1];
                    c--;
                    continue;


                }
                if(! podeEsquerda(grid,l,c) && !podeCima(grid,l,c)){
                    //fim, retornar soma
                    return somaFinal;
                }

            }
        }

        return 0;

    }


    private boolean podeEsquerda(int[][] grid, int l, int c) {

        //pode andar pra esquerda ?

        if(l>0){
            return true;
        }

        return false;

    }

    private boolean podeCima(int[][] grid, int l, int c) {

        //pode andar pra esquerda ?

        if(c>0){
            return true;
        }

        return false;

    }
}
